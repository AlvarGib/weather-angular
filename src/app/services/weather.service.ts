import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Weather } from '../models/interface-form';
import { apikey,URL  } from '../key/urls';


@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  
  private weather: Weather | any;
  private apikey = apikey;
  private listUrl = URL;

  weatherNow(){
    return this.weather;
  }

  constructor(private http: HttpClient) {
  
  }

  public getWeather(cityName: string, countryCode: string): Observable<Weather> {
    return this.http.get(`https://api.openweathermap.org/data/2.5/weather?lang=sp&appid=${ apikey }&units=metric&q=${ cityName }, ${ countryCode }`).pipe(map((response: Weather | any) =>{
      if(!response){
        throw new Error ('Value expected!');
      } else{
        return response;
        
      }
    }),
    catchError((err) => {
      throw new Error(err.message);
    })
    );
  }

  public getlist(): Observable<Weather[]> {
    return this.http.get(`http://localhost:3000/countryCode`).pipe(map((response: any) => {
        if(!response) {
          throw new Error ('Value expected!');
        }else {
          return response;
        }
      }),
      catchError(err =>{
        throw new Error(err.message);
      }),
      )
    }
  
}