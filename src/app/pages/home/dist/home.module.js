"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.HomeModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var home_routing_module_1 = require("./home-routing.module");
var home_component_1 = require("./home.component");
var gallery_component_1 = require("./gallery/gallery.component");
var angular_responsive_carousel_1 = require("angular-responsive-carousel");
var foto_component_1 = require("./gallery/foto/foto.component");
var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = __decorate([
        core_1.NgModule({
            declarations: [home_component_1.HomeComponent, gallery_component_1.GalleryComponent, foto_component_1.FotoComponent],
            imports: [
                common_1.CommonModule,
                home_routing_module_1.HomeRoutingModule,
                forms_1.ReactiveFormsModule,
                forms_1.FormsModule,
                angular_responsive_carousel_1.IvyCarouselModule
            ]
        })
    ], HomeModule);
    return HomeModule;
}());
exports.HomeModule = HomeModule;
