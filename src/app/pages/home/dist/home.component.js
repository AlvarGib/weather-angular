"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.HomeComponent = void 0;
var core_1 = require("@angular/core");
var HomeComponent = /** @class */ (function () {
    // @ViewChild("map") map: ElementRef;
    function HomeComponent(ws, renderer) {
        this.ws = ws;
        this.renderer = renderer;
        this.latitude = 18.5204;
        this.longitude = 73.8567;
        this.itemList = [];
    }
    // ngOnChanges(changes: SimpleChanges) {
    //   this.doSomething(changes.categoryId.currentValue);
    HomeComponent.prototype.ngOnChanges = function (cityName) {
        this.doSomething(cityName.inputcity.currentValue);
    };
    HomeComponent.prototype.doSomething = function (currentValue) {
        throw new Error('Method not implemented.');
    };
    HomeComponent.prototype.getItemList = function () {
        var _this = this;
        this.ws.getlist().subscribe(function (data) {
            _this.itemList = data;
        }, function (err) { console.error(err.message); });
        console.log(this.getItemList);
    };
    // this.map.forEach((inputcity: string, itemList: string) => {
    //   if (itemList == inputcity) {
    //     break
    //   })
    // }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getPosition().then(function (pos) {
            _this.mapRefresh(pos.lng, pos.lat);
        });
        this.gethomeWeather('madrid', "es");
    };
    HomeComponent.prototype.getPosition = function () {
        return new Promise(function (resolve, reject) {
            navigator.geolocation.getCurrentPosition(function (resp) {
                resolve({ lng: resp.coords.longitude, lat: resp.coords.latitude });
            }, function (err) {
                reject(err);
            });
        });
    };
    HomeComponent.prototype.gethomeWeather = function (cityName, countryCode) {
        var _this = this;
        this.ws.getWeather(cityName, countryCode).subscribe(function (data) {
            _this.homeWeather = data;
            _this.mapRefresh(_this.homeWeather.coord.lon, _this.homeWeather.coord.lat);
        }, function (err) {
            console.error(err.message);
        });
    };
    HomeComponent.prototype.submitLocation = function (cityName, countryCode) {
        if (cityName.value && countryCode.value) {
            this.gethomeWeather(cityName.value, countryCode.value);
            cityName.value = '';
            countryCode.value = '';
        }
        else {
            alert('Please. Insert some values');
        }
        cityName.focus();
        return false;
    };
    HomeComponent.prototype.setCenter = function () {
        var view = this.map.getView();
        view.setCenter(ol.proj.fromLonLat([this.longitude, this.latitude]));
        view.setZoom(14);
    };
    HomeComponent.prototype.mapRefresh = function (lon, lat) {
        // this.renderer.addClass(this.map.nativeElement, "map");
        var map = document.getElementById("map");
        if (map === undefined || map === null) {
            console.error("there is no map");
        }
        else {
            map.innerHTML = "";
        }
        ;
        // var mapIcono = document.getElementById("map_icono");
        // if (mapIcono === undefined || mapIcono === null){
        //   console.error("there is no map");
        // } else { mapIcono.innerHTML= ""};
        // };
        console.log("LAT: " + lat);
        console.log("Lon: " + lon);
        this.map = new ol.Map({
            target: 'map',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                })
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([lon, lat]),
                zoom: 14
            })
        });
        fetch('http://nominatim.openstreetmap.org/reverse?format=json&lon=' + lon + '&lat=' + lat).then(function (response) {
            return response.json();
        }).then(function (json) {
            console.log("Estas en" + " " + json.address.city);
        });
    };
    __decorate([
        core_1.Input()
    ], HomeComponent.prototype, "categoryId");
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'app-home',
            templateUrl: './home.component.html',
            styleUrls: ['./home.component.scss']
        })
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
