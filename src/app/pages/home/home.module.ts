import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { GalleryComponent } from './gallery/gallery.component';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { FotoComponent } from './gallery/foto/foto.component';



@NgModule({
  declarations: [HomeComponent, GalleryComponent, FotoComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    IvyCarouselModule
  ]
})
export class HomeModule { }
