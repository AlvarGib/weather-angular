import { Component, OnInit, Input } from '@angular/core';
import { Image } from './../models/i-gallery';
@Component({
  selector: 'app-foto',
  templateUrl: './foto.component.html',
  styleUrls: ['./foto.component.scss']
})
export class FotoComponent implements OnInit {
  @Input() image: Image | any;

  constructor() { }

  ngOnInit(): void {
  }

}
