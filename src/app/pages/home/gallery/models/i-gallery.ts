export interface Gallery {

    arrayImg: [],
    image: Image,

}
export interface Image {
    img: string,
    imgAlt: string,
    event: string,
}