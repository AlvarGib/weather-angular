import { Component, OnInit } from '@angular/core';
import { Gallery, Image } from './models/i-gallery';
import { trigger, transition, animate, style } from '@angular/animations';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
  animations: [

    trigger('myInsertRemoveTrigger', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('300ms', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('300ms', style({ opacity: 0 }))
      ]),
    ]),
  ],
})
export class GalleryComponent implements OnInit {

  texto: string | any;
  Gallery1: Gallery | any = {};
  Img1: Image | any = {};
  Img2: Image | any = {};
  Img3: Image | any = {};
  Img4: Image | any = {};
  Img5: Image | any = {};
  Img6: Image | any = {};
  Img7: Image | any = {};
  Img8: Image | any = {};
  Img9: Image | any = {};

  constructor() { 
    this.Img1.img = "assets/campo1.jpeg";
    this.Img1.imgAlt = "Descripcion";
    this.Img1.event = "CLICK";
    this.Img2.img = "assets/girasol.jpeg";
    this.Img2.imgAlt = "Descripcion";
    this.Img2.event = "ON MOUSE OVER";
    this.Img3.img = "assets/nube2.jpg";
    this.Img3.imgAlt = "Descripcion";
    this.Img4.img = "assets/hielo.jpeg";
    this.Img4.imgAlt = "Descripcion";
    this.Img5.img = "assets/nieve1.jpeg";
    this.Img5.imgAlt = "Descripcion";
    this.Img6.img = "assets/nieve2.jpeg";
    this.Img6.imgAlt = "Descripcion";
    this.Img7.img = "assets/nieve3.jpeg";
    this.Img7.imgAlt = "Descripcion";
    this.Img8.img = "assets/tormenta2.jpeg";
    this.Img8.imgAlt = "Descripcion";
    this.Img9.img = "assets/tormenta.jpeg";
    this.Img9.imgAlt = "Descripcion";
  }

  ngOnInit(): void {
  }
  public clickEvent() {
    this.texto = "";
    this.texto = "Has hecho click";


  }
  public dClickEvent() {


    this.texto = "Has hecho doble click";
  }
  public mouseEnterEnvent() {
    this.texto = "ha entrado el ratón";

  }
  public dragEvent() {
    debugger
    this.texto = "DRAG";

  }
  public activaGallery() {

  }
}
