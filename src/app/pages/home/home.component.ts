
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Weather } from 'src/app/models/interface-form';
import { WeatherService } from 'src/app/services/weather.service';
import { ViewChild, ElementRef, Renderer2 } from '@angular/core';

declare var ol: any;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @Input() categoryId: string | undefined;

  public homeWeather: Weather | any;

  latitude: number = 18.5204;
  longitude: number = 73.8567;

  map: '' |any;
  location: any;
  temp: '' | any;
  itemList: Weather[]= [];

  // @ViewChild("map") map: ElementRef;

  constructor(private ws: WeatherService, private renderer: Renderer2) { }

    
  // ngOnChanges(changes: SimpleChanges) {

  //   this.doSomething(changes.categoryId.currentValue);
    
  ngOnChanges(cityName: SimpleChanges) {

    this.doSomething(cityName.inputcity.currentValue);
  }
  doSomething(currentValue: any) {
    throw new Error('Method not implemented.');
  }

  public getItemList(): void {
    this.ws.getlist().subscribe((data: Weather[]) => {
      this.itemList = data;
    }, (err) => { console.error(err.message); }
    );
    console.log(this.getItemList);
  }

  // this.map.forEach((inputcity: string, itemList: string) => {
  //   if (itemList == inputcity) {
  //     break
  //   })
  // }



  ngOnInit(): void { 
  
  this.getPosition().then(pos => {
    this.mapRefresh(pos.lng,pos.lat,  );
    
    });

    this.gethomeWeather('madrid', "es");
  }

  getPosition(): Promise<any> {
    return new Promise((resolve, reject) => {

      navigator.geolocation.getCurrentPosition(resp => {

        resolve({ lng: resp.coords.longitude, lat: resp.coords.latitude });
      },
        err => {
          reject(err);
        });
    });

  }


  public gethomeWeather(cityName: string, countryCode: string): void{
    this.ws.getWeather(cityName, countryCode).subscribe((data: Weather) => {
    this.homeWeather = data;
      
    this.mapRefresh(this.homeWeather.coord.lon, this.homeWeather.coord.lat);
      
  },
    (err) => {
      console.error(err.message);
    })
}

  submitLocation(cityName: HTMLInputElement, countryCode: HTMLInputElement) {
    if (cityName.value && countryCode.value) {
      this.gethomeWeather(cityName.value, countryCode.value);
      cityName.value = '';
      countryCode.value = '';
    } else {
      alert('Please. Insert some values');
    }
    cityName.focus();
    return false; 
  
  }
  setCenter(): void {
    var view = this.map.getView();
    view.setCenter(ol.proj.fromLonLat([this.longitude, this.latitude]));
    view.setZoom(14);
  }

  mapRefresh(lon: '', lat: '') {
    // this.renderer.addClass(this.map.nativeElement, "map");
    var map = document.getElementById("map");
    if (map === undefined || map === null) { 
      console.error("there is no map");
    } else { map.innerHTML = "";};
    // var mapIcono = document.getElementById("map_icono");
    // if (mapIcono === undefined || mapIcono === null){
    //   console.error("there is no map");
    // } else { mapIcono.innerHTML= ""};
    // };
    console.log("LAT: "+lat);
    console.log("Lon: " + lon);
    this.map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([lon, lat]),
        zoom: 14
      })
      
    });
    fetch('http://nominatim.openstreetmap.org/reverse?format=json&lon=' + lon + '&lat=' + lat).then(function (response) {
        return response.json();
      }).then(function (json) {
        console.log("Estas en" + " " + json.address.city);
      });
    


}

  


}










