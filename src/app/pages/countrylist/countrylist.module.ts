import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CountrylistRoutingModule } from './countrylist-routing.module';
import { CountrylistComponent } from './countrylist.component';
import { PaisesComponent } from './paises/paises.component';


@NgModule({
  declarations: [CountrylistComponent, PaisesComponent],
  imports: [
    CommonModule,
    CountrylistRoutingModule
  ]
})
export class CountrylistModule { }
