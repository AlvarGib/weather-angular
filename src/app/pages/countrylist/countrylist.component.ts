import { Component, OnInit } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';
import { Location } from '@angular/common';
import { Weather } from 'src/app/models/interface-form';

@Component({
  selector: 'app-countrylist',
  templateUrl: './countrylist.component.html',
  styleUrls: ['./countrylist.component.scss']
})
export class CountrylistComponent implements OnInit {

  public itemList: Weather[] = [];
  

  constructor(private ws: WeatherService, private location: Location) { }

  ngOnInit(): void {
    this.getItemList()
  }
  
  public getItemList(): void {
    this.ws.getlist().subscribe((data: Weather[]) => {
      this.itemList = data;
    }, (err) => { console.error(err.message); }
    );
  }
  

  
  public goBack(): void {
    this.location.back();
  }
}
