"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CountrylistComponent = void 0;
var core_1 = require("@angular/core");
var CountrylistComponent = /** @class */ (function () {
    function CountrylistComponent(ws, location) {
        this.ws = ws;
        this.location = location;
        this.itemList = [];
    }
    CountrylistComponent.prototype.ngOnInit = function () {
        this.getItemList();
    };
    CountrylistComponent.prototype.getItemList = function () {
        var _this = this;
        this.ws.getlist().subscribe(function (data) {
            _this.itemList = data;
        }, function (err) { console.error(err.message); });
    };
    CountrylistComponent.prototype.goBack = function () {
        this.location.back();
    };
    CountrylistComponent = __decorate([
        core_1.Component({
            selector: 'app-countrylist',
            templateUrl: './countrylist.component.html',
            styleUrls: ['./countrylist.component.scss']
        })
    ], CountrylistComponent);
    return CountrylistComponent;
}());
exports.CountrylistComponent = CountrylistComponent;
