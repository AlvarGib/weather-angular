import { Component, OnInit, Input } from '@angular/core';
import { Weather } from 'src/app/models/interface-form';

@Component({
  selector: 'app-paises',
  templateUrl: './paises.component.html',
  styleUrls: ['./paises.component.scss']
})
export class PaisesComponent implements OnInit {

    @Input() item: Weather[] | any;

  constructor() { }

  ngOnInit(): void {
  }

}
