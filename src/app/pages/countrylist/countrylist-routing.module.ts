import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CountrylistComponent } from './countrylist.component';

const routes: Routes = [{ path: '', component: CountrylistComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CountrylistRoutingModule { }
