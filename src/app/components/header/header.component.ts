import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public imageLogo: string = 'assets/logo8.png';
  public altLogo: string = 'el Tiempo Logo';
  constructor() { }

  ngOnInit(): void {
  }

}
